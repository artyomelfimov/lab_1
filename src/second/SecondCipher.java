package second;

import main.CipherStrategy;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

// Кодировочная книга
// Java / Intellij Idea
public class SecondCipher implements CipherStrategy {
    private Scanner sc = new Scanner(System.in);

    public String execute(String string) {
        try {
            return encrypt(string);
        } catch (IOException e) {
            System.out.println(e.getMessage());
            return "";
        }

    }


    private String encrypt(String message) throws IOException {
        List<String[]> lines = createMatrixFromFile();

        int maxStr = lines.size();
        int maxPosition = getMaxPosition(lines);

        return getResultAutomatically(lines, maxStr, maxPosition, message);

    }

    private String getResultAutomatically(List<String[]> lines, int maxStr, int maxPosition, String string) {
        boolean found;
        int max = Math.max(maxStr, maxPosition);
        int numberOfDigits = getNumberOfNumber(max);
        StringBuilder sb = new StringBuilder();
        Map<String, Integer> usedWordsMap = new HashMap<>();
        for (String str : string.split(" ")) {
            found = false;
            List<String> usedWordsList = new ArrayList<>();
            for (int j = 0; j < lines.size(); j++) {
                for (int i = 0; i < lines.get(j).length; i++) {
                    if (str.equals(lines.get(j)[i])) {
                        StringBuilder positionBuilder = getPositionBuilder(numberOfDigits, j, i);

                        if (!usedWordsMap.containsKey(positionBuilder.toString())) {
                            usedWordsMap.put(positionBuilder.toString(), 1);
                            sb.append(positionBuilder);
                            found = true;
                            break;
                        } else {
                            usedWordsList.add(positionBuilder.toString());
                        }
                    }
                }
                if (found) {
                    break;
                }
            }

            if (!found && usedWordsList.size() != 0) {
                boolean isAllEquals = isAllWordsHaveEqualsRepeats(usedWordsMap, usedWordsList);

                if (isAllEquals) {
                    sb.append(usedWordsList.get(0));
                    usedWordsMap.put(usedWordsList.get(0), usedWordsMap.get(usedWordsList.get(0)) + 1);
                    continue;
                }

                String key = findStringWithMinRepeats(usedWordsMap, usedWordsList);
                sb.append(key);
                usedWordsMap.put(key, usedWordsMap.get(key) + 1);
                continue;
            }

            if (!found) {
                System.out.println("Word not found in book.txt");
                return "";
            }
        }

        return sb.toString();
    }

    private String findStringWithMinRepeats(Map<String, Integer> usedWordsMap, List<String> usedWordsList) {
        int minValue = Integer.MAX_VALUE;
        String key = "";

        for (String s : usedWordsList) {
            Integer value = usedWordsMap.get(s);
            if (value < minValue) {
                minValue = value;
                key = s;
            }
        }
        return key;
    }

    private boolean isAllWordsHaveEqualsRepeats(Map<String, Integer> usedWordsMap, List<String> usedWordsList) {
        int val = usedWordsMap.get(usedWordsList.get(0));
        boolean isAllEquals = true;
        for (String s : usedWordsList) {
            Integer value = usedWordsMap.get(s);
            if (value != val) {
                isAllEquals = false;
                break;
            }
        }
        return isAllEquals;
    }

    private StringBuilder getPositionBuilder(int numberOfDigits, int j, int i) {
        StringBuilder positionBuilder = new StringBuilder();

        String s = addZeros(numberOfDigits, j + 1);
        s = s.concat("" + (j + 1));
        positionBuilder.append(s);

        s = addZeros(numberOfDigits, i + 1);
        s = s.concat("" + (i + 1));
        positionBuilder.append(s);

        return positionBuilder;
    }

    private String addZeros(int numberOfDigits, int currentNumber) {
        int currentNum = getNumberOfNumber(currentNumber);
        String s = "";
        while (currentNum < numberOfDigits) {
            currentNum++;
            s = s.concat("0");
        }
        return s;
    }

    private int getMaxPosition(List<String[]> lines) {
        return lines.stream()
                .mapToInt(line -> line.length)
                .max()
                .getAsInt();
    }

    private List<String[]> createMatrixFromFile() throws IOException {
        Pattern pattern = Pattern.compile("[\\w']+|[.,!?;]");
        List<String[]> lines = new ArrayList<>();
        BufferedReader bufferedReader = new BufferedReader(new FileReader(
                ("E:/book.txt")));
        bufferedReader
                .lines()
                .map(String::toLowerCase)
                .map(line -> line.replaceAll(",", " ,"))
                .map(line -> line.replaceAll("\\?", " ?"))
                .map(line -> line.replaceAll("\\.", " ."))
                .map(line -> line.split(" "))
                .forEach(lines::add);
        bufferedReader.close();
        return lines;
    }

    private int getNumberOfNumber(int number) {
        int counter = 0;
        while (number > 0) {
            number /= 10;
            counter++;
        }

        return counter;
    }

    private List<Tuple> readWordsPositions(int maxString, int maxPosition, int numberOfWords) {
        Scanner sc = new Scanner(System.in);
        List<Tuple> positions = new ArrayList<>();
        int counter = 0;

        while (true) {
            System.out.print("Enter string number: ");
            int stringNumber = sc.nextInt();
            System.out.print("Enter position number: ");
            int positionNumber = sc.nextInt();

            if (stringNumber > maxString || stringNumber < 1) {
                System.out.println("Incorrect string number");
                continue;
            }

            if (positionNumber > maxPosition || positionNumber < 1) {
                System.out.println("Incorrect position number");
                continue;
            }

            positions.add(new Tuple(stringNumber, positionNumber));
            counter++;
            if (counter == numberOfWords) {
                return positions;
            }
        }
    }
}

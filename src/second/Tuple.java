package second;

import java.util.Objects;

public class Tuple {
    private int rowNumber;
    private int positionNumber;

    public Tuple(int rowNumber, int positionNumber) {
        this.rowNumber = rowNumber;
        this.positionNumber = positionNumber;
    }

    public int getRowNumber() {
        return rowNumber;
    }

    public void setRowNumber(int rowNumber) {
        this.rowNumber = rowNumber;
    }

    public int getPositionNumber() {
        return positionNumber;
    }

    public void setPositionNumber(int positionNumber) {
        this.positionNumber = positionNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tuple tuple = (Tuple) o;
        return rowNumber == tuple.rowNumber &&
                positionNumber == tuple.positionNumber;
    }

    @Override
    public int hashCode() {
        return Objects.hash(rowNumber, positionNumber);
    }
}

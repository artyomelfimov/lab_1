package first;

import main.CipherStrategy;

import java.util.Arrays;
import java.util.Scanner;

// Шифрование перестановкой
public class FirstCipher implements CipherStrategy {
    private static Scanner sc = new Scanner(System.in);

    public String execute(String string) {
        return encrypt(string);
    }


    private String encrypt(String string) {

        int rows;
        int columns;

        int[] calculations = getCalculatedRows(string);
        rows = calculations[0];
        columns = calculations[1];

        System.out.println(rows);
        System.out.println(columns);

        char[][] matrix = new char[rows][columns];


        if (rows * columns > string.length()) {
            int length = string.length();
            int numberOfSpaces = rows * columns - length;
            string = appendSpacesToString(string, numberOfSpaces);
        }

        int[] orderOfRows = getIntArray(rows, "Enter order of rows");
        int[] orderOfColumns = getIntArray(columns, "Enter order of columns");

        char[] stringInChar = string.toCharArray();

        int counter1 = 0;
        int counter2 = 0;
        for (int i = 0; i < stringInChar.length; i++) {
            matrix[orderOfRows[counter1] - 1][counter2] = stringInChar[i];
            counter2++;
            if (counter2 == columns) {
                counter2 = 0;
                counter1++;
            }

            if (counter1 == rows) {
                break;
            }
        }


        return combineString(matrix, orderOfColumns);
    }

    private int[] getIntArray(int size, String description) {
        int[] array = new int[size];
        System.out.println(description + ": ");
        for (int i = 0; i < size; i++) {
            array[i] = sc.nextInt();
        }

        return array;
    }

    private String appendSpacesToString(String string, int numberOfSpaces) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < numberOfSpaces; i++) {
            sb.append(" ");
        }
        return string.concat(sb.toString());
    }

    private String combineString(char[][] matrix, int[] orderOfColumns) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < matrix[0].length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                sb.append(matrix[j][orderOfColumns[i] - 1]);
            }
        }

        return sb.toString();
    }

    private int[] getCalculatedRows(String string) {
        int length = string.length();
        int possibleRows = (int) Math.sqrt(length);
        int columns = possibleRows;
        for (int i = possibleRows; i * columns <= length ; i++) {
            possibleRows = i;
        }
        return new int[] {possibleRows, columns};
    }
}

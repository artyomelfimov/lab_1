package main;

import java.util.Scanner;

public class ConsoleInput implements InputStrategy {
    @Override
    public String getString() {
        Scanner scanner =  new Scanner(System.in);
        System.out.println("Enter string: ");
        return scanner.nextLine();
    }
}

package main;

import java.io.*;
import java.util.stream.Collectors;

public class FileInput implements InputStrategy {
    @Override
    public String getString() {
        try {
            BufferedReader reader = new BufferedReader(new FileReader("E:/text.txt"));
            return reader.lines().collect(Collectors.joining());
        } catch (IOException e) {
            System.out.println(e.getMessage());
            return "";
        }
    }
}

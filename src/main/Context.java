package main;

public class Context {
    private CipherStrategy cipherStrategy;
    private InputStrategy inputStrategy;

    public void setCipherStrategy(CipherStrategy cipherStrategy) {
        this.cipherStrategy = cipherStrategy;
    }

    public String executeCipherStrategy(String string) {
        return cipherStrategy.execute(string);
    }

    public void setInputStrategy(InputStrategy inputStrategy) {
        this.inputStrategy = inputStrategy;
    }

    public String executeInputStrategy() {
        return inputStrategy.getString();
    }
}

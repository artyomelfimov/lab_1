package main;

public interface CipherStrategy {
    String execute(String string);
}

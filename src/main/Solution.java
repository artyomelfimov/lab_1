package main;

import first.FirstCipher;
import second.SecondCipher;
import third.CaesarCipher;

import java.util.Scanner;

public class Solution {
    private static Scanner scanner = new Scanner(System.in);
    private static Context context = new Context();

    public static void main(String[] args) {
        selectInputStrategy();
        selectCipherStrategy();

        String string = context.executeInputStrategy();
        String result = context.executeCipherStrategy(string);
        System.out.println(result);
    }

    private static void selectCipherStrategy() {
        System.out.println("Select algorithm: ");
        System.out.println("1. Шифр перестановкой");
        System.out.println("2. Кодировочная книга");
        System.out.println("3. Шифр Цезаря");
        int choice = scanner.nextInt();

        switch (choice) {
            case 1:
                context.setCipherStrategy(new FirstCipher());
                break;
            case 2:
                context.setCipherStrategy(new SecondCipher());
                break;
            case 3:
                context.setCipherStrategy(new CaesarCipher());
                break;
        }
    }

    private static void selectInputStrategy () {
        System.out.println("Select input strategy: ");
        System.out.println("1. From console");
        System.out.println("2. From file");
        int choice = scanner.nextInt();

        switch (choice) {
            case 1:
                context.setInputStrategy(new ConsoleInput());
                break;
            case 2:
                context.setInputStrategy(new FileInput());
                break;
        }
    }
}

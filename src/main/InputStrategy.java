package main;

public interface InputStrategy {
    String getString();
}
